from multiprocessing import context
from django.shortcuts import render , get_object_or_404
from django.http import HttpResponse
from django.template import  loader
from .models import Warrior

# Create your views here.
def index(request):
    heroquery = Warrior.objects.all()
    webtemplate = loader.get_template('game/index.html')
    context = {'mywarrios': heroquery}
    return HttpResponse(webtemplate.render(context, request))


def form(request):
    webtemplate =loader.get_template('game/submit.html')
    return HttpResponse(webtemplate.render({}, request))

def detail(request,warrior_id):
    warrior = get_object_or_404(Warrior,pk=warrior_id)
    context ={'Warrior':warrior}
    return render(request,'game/detail.html', context)
