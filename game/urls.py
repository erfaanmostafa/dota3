from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('select',views.form),
    path('detail/<int:warrior_id>',views.detail,name='detail'),
    ]