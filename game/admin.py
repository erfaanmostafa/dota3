from django.contrib import admin
from .models import Warrior

# Register your models here.
admin.site.register(Warrior)
